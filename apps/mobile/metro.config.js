const { withNxMetro } = require('@nx/expo')
const { getDefaultConfig } = require('@expo/metro-config')
const { mergeConfig } = require('metro-config')
const exclusionList = require('metro-config/src/defaults/exclusionList')

const defaultConfig = getDefaultConfig(__dirname)
const { assetExts, sourceExts } = defaultConfig.resolver

const customConfig = {
	transformer: {
		babelTransformerPath: require.resolve('react-native-svg-transformer')
	},
	resolver: {
		assetExts: assetExts.filter(ext => ext !== 'svg'),
		sourceExts: [...sourceExts, 'svg'],
		blockList: exclusionList([/^(?!.*node_modules).*\/dist\/.*/])
		// unstable_enableSymlinks: true,
		// unstable_enablePackageExports: true,
	}
}

module.exports = withNxMetro(mergeConfig(defaultConfig, customConfig), {
	debug: false,
	extensions: [],
	watchFolders: []
})
