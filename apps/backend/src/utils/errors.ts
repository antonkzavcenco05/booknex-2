export enum ErrorsEnum {
	Not_Enough_Rights = 'Not enough rights',
	Not_Found = 'Not found',
	Already_Exist = 'Already exist',
	Invalid_Value = 'Invalid value',
	Unknow_Error = 'Unknow error, please try again later'
}
